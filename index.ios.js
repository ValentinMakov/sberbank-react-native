import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

import App from "./components/App"

console.disableYellowBox = true;

export default class LoadVacancies extends Component {
  render() {
    return (
      <App />
    );
  }
}

AppRegistry.registerComponent('LoadVacancies', () => LoadVacancies);