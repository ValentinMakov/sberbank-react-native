import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';

import { InfiniteListView } from "react-native-infinite-listview";
import noIcon from "../img/noIcon.png";
import searchIcon from "../img/searchIcon.png";
import styles from "./styles";

// console.disableYellowBox = true;

const options = {
  headers: {
    "User-Agent": "ShowVacanciesApp"
  }
};

let vacanciesRawDataArray = [];
let perPageInitial = 80;
let perPageFurther = 20;
let counter = perPageInitial / perPageFurther - 1;
let submitText;

export default class App extends Component {
  state = {
    isRefreshing: false,
    isLoadingMore: false,
    listItems: []
  };

  // Refresh the list: clear search query variable (submitText), clear the source array in the state (listItems)
  onRefresh = () => {
    this.setState(
      { isRefreshing: true }
    );
    counter = perPageInitial / perPageFurther - 1;
    submitText = "";
    setTimeout(() => {
      this.setState(
        { isRefreshing: false, listItems: [], text: "" }
      );
    }, 3000);
  };

  // Check if the app have to upload more vacancies (up to 500, but this is a purely arbitrary number)
  canLoadMoreContent = () => {
    return this.state.listItems.length && this.state.listItems.length < 500 && !this.state.isLoadingMore;
  };

  // Loading more vacancies; counter variable allows to control the pagination, submitText variable contains the search query
  onLoadMore = () => {
    ++counter;
    console.log(`counter is ${counter}`);
    this.setState({ isLoadingMore: true });
    setTimeout(() => {
      fetch(`https://api.hh.ru/vacancies?text=${submitText}&only_with_salary=true&page=0&per_page=${perPageInitial}`, options)
        .then(response => response.json())
        .then(wholeVacancies => wholeVacancies.items.map(item => {
          return {
            vacancyName: item.name,
            icon: item.employer.logo_urls ? { uri: item.employer.logo_urls[90] } : noIcon,
            employer: item.employer.name,
            salaryLow: item.salary.from,
            salaryHigh: item.salary.to,
            currency: item.salary.currency
          }
        }))
        .then(filteredVacancies => {
          this.setState({ isLoadingMore: false, listItems: [...this.state.listItems, ...filteredVacancies] });
        })
        .catch(err => console.error("error fetching products", err));
    }, 3000);
  }

  // Search vacancies using the on word search query; if no results are found, a special version of the results array is returned
  submit = () => {
    this.setState({ isLoadingMore: true });
    submitText = this.state.text;
    fetch(`https://api.hh.ru/vacancies?text=${submitText}&only_with_salary=true&page=0&per_page=${perPageInitial}`, options)
      .then(response => response.json())
      .then(wholeVacancies => wholeVacancies.items.map(item => {
        return {
          vacancyName: item.name,
          icon: item.employer.logo_urls ? { uri: item.employer.logo_urls[90] } : noIcon,
          employer: item.employer.name,
          salaryLow: item.salary.from,
          salaryHigh: item.salary.to,
          currency: item.salary.currency
        }
      }))
      .then(filteredVacancies => {
        if (filteredVacancies.length !== 0) {
          vacanciesRawDataArray = filteredVacancies;
          this.setState({ listItems: vacanciesRawDataArray, isLoadingMore: false, text: "" });
        } else {
          this.setState({ listItems: [{ noResults: true }], isLoadingMore: false, text: "" });
          console.log(this.state.listItems);
        }
      })
      .catch(err => console.error("error fetching products", err));
  }

  // Rendering each vacancy, or no results message
  renderRow = (rowData, sectionID, rowID) => {
    function printCurrency(currency) {
      if (currency == "RUR") {
        return "руб."
      } else if (currency == "EUR") {
        return "евро"
      } else if (currency == "USD") {
        return "долл."
      } else {
        return currency
      }
    }
    if (!rowData.noResults) {
      return (
        <View style={styles.container}>
          <Image style={styles.companyLogo} source={rowData.icon} />
          <View style={styles.companyContainer}>
            <Text style={styles.vacancyName}>{rowData.vacancyName}</Text>
            <Text style={styles.companyName}>{rowData.employer}</Text>
            <Text style={styles.salary}>{
              (rowData.salaryLow && rowData.salaryHigh) ? `${rowData.salaryLow} — ${rowData.salaryHigh} ${printCurrency(rowData.currency)}` :
                rowData.salaryLow ? `${rowData.salaryLow} ${printCurrency(rowData.currency)}` : `${rowData.salaryHigh} ${printCurrency(rowData.currency)}`
            }</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.noResultsContainer}>
          <Text style={styles.noResultsText}>Ничего не нашлось</Text>
        </View>);
    }
  }

  // The infinite list of vacancies is built
  render() {
    return (
      <InfiniteListView
        style={styles.scrollView}
        dataArray={this.state.listItems}
        renderHeader={() => (
          <View style={styles.queryRow}>
            <TextInput
              style={styles.input}
              placeholder={"Введите запрос"}
              onChangeText={(text) => this.setState({ text })}
              value={this.state.text}
            />
            <TouchableOpacity style={styles.buttonContainer}
              onPress={this.submit}>
              <Image
                style={styles.button}
                source={searchIcon} />
            </TouchableOpacity>
          </View>
        )}
        renderRow={this.renderRow}
        onRefresh={this.onRefresh}
        isRefreshing={this.state.isRefreshing}
        isLoadingMore={this.state.isLoadingMore}
        onLoadMore={this.onLoadMore}
        canLoadMore={this.canLoadMoreContent}
      />
    );
  }
}
