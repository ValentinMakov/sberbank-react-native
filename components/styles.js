import React, { Component } from 'react';
import {
  StyleSheet,
} from 'react-native';

export default StyleSheet.create({
    scrollView: {
      flex: 1,
      paddingTop: 20,
      backgroundColor: "#E8E8E8"
    },
    container: {
      flexDirection: "row",
      flex: 1,
      justifyContent: "flex-start",
      // alignItems: "flex-start",
      backgroundColor: "white",
      padding: 10,
      borderBottomWidth: StyleSheet.hairlineWidth,
      borderBottomColor: "lightgrey"
    },
    vacancyName: {
      fontSize: 18,
      fontWeight: "bold",
      textAlign: "left",
      // flex:1,
      flexWrap: "wrap"
      // margin: 10,
    },
    companyContainer: {
      flex: 1
    },
    companyLogo: {
      height: 60,
      width: 60,
      resizeMode: "contain",
      marginRight: 10,
      alignSelf: "center"
    },
    companyName: {
      fontSize: 18,
      // flex: 1,
      flexWrap: "wrap"
    },
    salary: {
      fontSize: 20,
      fontWeight: "bold"
    },
    queryRow: {
      // padding: 5,
      backgroundColor: "white",
      flexDirection: "row",
      flex: 1,
      borderBottomWidth: 1,
      borderBottomColor: "lightgrey"
    },
    input: {
      height: 50,
      fontSize: 25,
      backgroundColor: "white",
      padding: 5,
      flex: 1
    },
    buttonContainer: {
      alignSelf: "center",
      marginRight: 10
    },
    button: {
      width: 25,
      height: 25,
    },
    noResultsContainer: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center"
    },
    noResultsText: {
      fontSize: 30
    }
  });